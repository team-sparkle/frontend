import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './app'
import * as serviceWorker from './serviceWorker'
import HTTPService from './service/http-service'

const httpService = new HTTPService()
httpService.fetchActivities()

const renderApp = () => {
  ReactDOM.render(<App httpService={httpService} />, document.getElementById('root'))
}

httpService.subscribe(renderApp)
renderApp()

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
