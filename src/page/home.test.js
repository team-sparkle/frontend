import React from 'react'
import { configure, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Home from './home'

configure({ adapter: new Adapter() })

describe('<Home />', () => {
  it('should mount without crashing', () => {
    mount(<Home />)
  })
})
