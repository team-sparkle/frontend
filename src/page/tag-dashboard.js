import React, { Component } from 'react'
import Tag from '../component/tag'

class TagDashboard extends Component {
  constructor (props) {
    super(props)
    this.state = {
      httpService: this.props.httpService,
      tags: []
    }
  }

  componentDidMount () {
    this.state.httpService.getTags()
      .then((tags) => {
        this.setState({ tags: tags })
      })
  }

  render () {
    return (
      <div id='Dashboard'>
        <h1 className='whiteText'>Tags</h1>
        <div className='activities'>
          {((typeof (this.state.tags) !== 'undefined' && this.state.tags.length > 0) && this.state.tags.map((tag, i) => {
            return <Tag key={i} tagID={tag.id} tagName={tag.tag} />
          }))}
          {((typeof (this.state.tags) === 'undefined' || this.state.tags.length === 0) &&
            <h1 className='whiteText'>No Tags Found!</h1>
          )}
        </div>
      </div>
    )
  }
}

export default TagDashboard
