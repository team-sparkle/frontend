import React, { Component } from 'react'

class Home extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  render () {
    return (
      <div id='Home'>
        <h1 className='whiteText'>Home</h1>
      </div>
    )
  }
}

export default Home
