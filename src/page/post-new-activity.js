import React, { Component } from 'react'
import { Button } from '../component/button'
import './post-new-activity.css'

class PostNewActivity extends Component {
  constructor (props) {
    super(props)
    this.state = {
      httpService: this.props.httpService,
      title: '',
      imgURL: '',
      description: '',
      materials: [''],
      instructions: '',
      timeEstimate: '',
      tagsRaw: '',
      tags: ['']
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleAppendInputMaterials = this.handleAppendInputMaterials.bind(this)
    this.handleAppendInputTags = this.handleAppendInputTags.bind(this)
    this.handleChangeMultiple = this.handleChangeMultiple.bind(this)
  }

  handleChange (e) {
    const name = e.target.name
    const value = e.target.value
    this.setState({ [name]: value })
  }

  handleChangeMultiple = index => e => {
    e.preventDefault()
    const value = e.target.value
    const name = e.target.name
    const prevArray = this.state[name]
    prevArray[index] = value
    this.setState({
      [name]: prevArray
    })
  }

  handleAppendInputMaterials (e) {
    e.preventDefault()
    this.setState(prevState => ({ materials: prevState.materials.concat(['']) }))
  }

  handleAppendInputTags (e) {
    e.preventDefault()
    this.setState(prevState => ({ tags: prevState.tags.concat(['']) }))
  }

  handleSubmit (event) {
    event.preventDefault()
    const data = {
      title: this.state.title,
      imgURL: this.state.imgURL,
      description: this.state.description,
      materials: this.state.materials.toString(),
      instructions: this.state.instructions,
      timeEstimate: this.state.timeEstimate,
      tags: this.state.tags
    }
    this.state.httpService.postNewActivity(data).then(() => {
      window.alert('Thank you for your contribution to Kid\'s Fort!')
      this.setState({
        title: '',
        description: '',
        materials: [''],
        instructions: '',
        timeEstimate: '',
        tagsRaw: '',
        tags: ['']
      })
      return false
    }).catch((err) => {
      window.alert(`OOPS! Something went wrong!\n${err}`)
      return false
    })
  }

  render () {
    return (
      <div id='postNewActivities'>
        <h1 className='whiteText'>Post A New Activity</h1>
        <div className='postActivity'>
          <form className='postActivityForm' onSubmit={this.handleSubmit}>
            <div className='card'>
              <label>
                <div className='left'>
                  Title
                  <span className='red'> *</span>
                </div>
                <br />
                <input
                  required
                  value={this.state.title}
                  name='title'
                  onChange={this.handleChange}
                />
              </label>
            </div>
            <br />
            <div className='card'>
              <label>
                <div className='left'>
                  Image URL
                  <span className='red'> *</span>
                </div>
                <br />
                <input
                  required
                  value={this.state.imgURL}
                  name='imgURL'
                  onChange={this.handleChange}
                />
              </label>
            </div>
            <br />
            <div className='card'>
              <label>
                <div className='left'>
                Description
                  <span className='red'> *</span>
                </div>
                <br />
                <textarea
                  required
                  value={this.state.description}
                  name='description'
                  onChange={this.handleChange}
                />
              </label>
            </div>
            <br />
            <div>
              <label>
                <div className='left'>
                Materials
                  <span className='red'> *</span>
                </div>
                <br />
                <div className='dynamicInputWrapper'>
                  {this.state.materials.map((input, i) => <div key={i} className='dynamicInput'><input required key={i} value={this.state.materials[i]} name='materials' onChange={this.handleChangeMultiple(i)} /></div>)}
                  <div className='dynamicInput'>
                    <button className='addField' onClick={this.handleAppendInputMaterials}>
                        +
                    </button>
                  </div>
                </div>
              </label>
            </div>
            <br />
            <div className='card'>
              <label>
                <div className='left'>
                Instructions
                  <span className='red'> *</span>
                </div>
                <br />
                <textarea
                  required
                  value={this.state.instructions}
                  name='instructions'
                  onChange={this.handleChange}
                />
              </label>
            </div>
            <br />
            <div className='card'>
              <label>
                <div className='left'>
                Time Estimate (in minutes)
                  <span className='red'> *</span>
                </div>
                <br />
                <input
                  required
                  value={this.state.timeEstimate}
                  type='number'
                  name='timeEstimate'
                  onChange={this.handleChange}
                />
              </label>
            </div>
            <br />
            <div>
              <label>
                <div className='left'>
                Tags
                  <span className='red'> *</span>
                </div>
                <br />
                <div className='dynamicInputWrapper'>
                  {this.state.tags.map((input, i) => <div key={i} className='dynamicInput'><input required key={i} value={this.state.tags[i]} name='tags' onChange={this.handleChangeMultiple(i)} /></div>)}
                  <div className='dynamicInput'>
                    <button className='addField' onClick={this.handleAppendInputTags}>
                        +
                    </button>
                  </div>
                </div>
              </label>
            </div>
            <br />
            <Button primary type='submit' value='Submit' label='Submit' />
          </form>
        </div>
      </div>
    )
  }
}

export default PostNewActivity
