import React, { Component } from 'react'
import Activity from '../../component/activity'
import './dashboard.css'
import { Button } from '../../component/button'

class Dashboard extends Component {
  constructor (props) {
    super(props)
    this.state = {
      activities: this.props.activities,
      httpService: this.props.httpService,
      tagID: this.props.tagID,
      tagName: ''
    }
  }

  componentDidMount () {
    this.state.httpService.getTagNameByID(this.state.tagID)
      .then((tagName) => {
        if (tagName !== '') {
          this.setState({ tagName: tagName })
        }
      })
  }

  render () {
    return (
      <div id='Dashboard'>
        {this.state.tagName !== '' &&
          <h1 className='whiteText'>{`Tag: ${this.state.tagName}`}</h1>}
        <div className='activities'>
          {((typeof (this.state.activities) !== 'undefined' && this.state.activities.length > 0) && this.state.activities.map((activity, i) => {
            return <Activity key={i} activity={activity} httpService={this.state.httpService} />
          }))}
          {((typeof (this.state.activities) === 'undefined' || this.state.activities.length === 0) &&
            <h1 className='whiteText'>No Activities Found!</h1>
          )}
        </div>
        {this.state.tagName !== '' &&
          <Button secondary to='/tags' label='Back to Tags' />}
      </div>
    )
  }
}

export default Dashboard
