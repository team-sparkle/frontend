import React, { Component } from 'react'
import { Button } from '../../component/button'
import './activity-details.css'
import { NavLink } from 'react-router-dom'
import LikeDislike from '../../component/like-dislike'

class ActivityDetails extends Component {
  constructor (props) {
    super(props)
    this.state = {
      httpService: this.props.httpService,
      activity: this.props.activity
    }
  }

  render () {
    if (this.state.activity) {
      return (
        <div className='activityDetailsWrapper'>
          <div className='activityDetails'>
            <img
              className='activityDetailsImage'
              alt={`activity: ${this.state.activity.title}`}
              src={this.state.activity.imgURL}
            />
            <div className='activityDetailsContents'>
              <h1>{this.state.activity.title}</h1>
              <LikeDislike
                activityID={this.state.activity.id}
                httpService={this.state.httpService}
                likes={this.state.activity.likes}
                dislikes={this.state.activity.dislikes}
              />
              <h2>Description</h2>
              <p>{this.state.activity.description}</p>
              <h2>Materials</h2>
              <p>{this.state.activity.materials}</p>
              <h2>Instructions</h2>
              <p>{this.state.activity.instructions}</p>
              <h2>Time Estimate</h2>
              <p>{this.state.activity.timeEstimate} minutes</p>
              <p>Posted on {(new Date(this.state.activity.dateCreated)).toLocaleString()}</p>
              <p>Tags: {this.state.activity.tags.map((tag, i) => {
                return (
                  <NavLink key={i} to={`/dashboard/${tag.id}`}>
                    {tag.tag},
                  </NavLink>
                )
              })}
              </p>
              <Button primary to='/' label='Back to Home' />
            </div>
          </div>
        </div>
      )
    } else {
      return (
        <div className='activity'>
          <h1>Activity Not Found!</h1>
          <Button primary to='/' label='Back to Home' />
        </div>
      )
    }
  }
}

export default ActivityDetails
