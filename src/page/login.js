import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { Button } from '../component/button'

class Login extends Component {
  constructor (props) {
    super(props)
    this.state = {
      httpService: this.props.httpService,
      username: '',
      password: '',
      redirect: false
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.renderRedirect = this.renderRedirect.bind(this)
  }

  handleChange (e) {
    const name = e.target.name
    const value = e.target.value
    this.setState({ [name]: value })
  }

  handleSubmit (event) {
    event.preventDefault()
    this.state.httpService.login(this.state.username, this.state.password).then(() => {
      this.setState({
        username: '',
        password: '',
        redirect: true
      })
      return false
    }).catch((err) => {
      window.alert(`OOPS! Something went wrong!\n${err}`)
      return false
    })
  }

  renderRedirect () {
    if (typeof (this.state.httpService) !== 'undefined' && this.state.httpService.loggedIn()) {
      return <Redirect to='/' />
    }
  }

  render () {
    return (
      <div id='postNewActivities'>
        {this.renderRedirect()}
        <h1 className='whiteText'>Login</h1>
        <div className='postActivity'>
          <form className='postActivityForm' onSubmit={this.handleSubmit}>
            <div className='card'>
              <label>
                <div className='left'>
                  Username
                  <span className='red'> *</span>
                </div>
                <br />
                <input
                  required
                  value={this.state.username}
                  name='username'
                  onChange={this.handleChange}
                />
              </label>
            </div>
            <br />
            <div className='card'>
              <label>
                <div className='left'>
                  Password
                  <span className='red'> *</span>
                </div>
                <br />
                <input
                  required
                  type='password'
                  value={this.state.password}
                  name='password'
                  onChange={this.handleChange}
                />
              </label>
            </div>
            <br />
            <Button primary type='submit' value='Login' label='Login' />
          </form>
        </div>
      </div>
    )
  }
}

export default Login
