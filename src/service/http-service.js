import decode from 'jwt-decode'

const API_SERVER_URL = 'http://localhost:8080/'
const filter = require('lodash.filter')
const find = require('lodash.find')
const flatMapDeep = require('lodash.flatmapdeep')
const uniqBy = require('lodash.uniqby')

class HTTPService {
  activities = []

  fetchActivities () {
    return this._makeRequest('activities', {
      method: 'GET'
    }).then(activities => {
      this.activities = activities
      this._handleChange()
    }).catch(error => console.error('parsing failed', error))
  }

  getActivities () {
    return this.activities
  }

  getActivitiesByTagID (tagID) {
    if (typeof (tagID) !== 'undefined') {
      return filter(this.activities, { tags: [{ id: Number(tagID) }] })
    } else {
      return this.getActivities()
    }
  }

  getTagNameByID (tagID) {
    return new Promise((resolve, reject) => {
      const activity = find(this.activities, { tags: [{ id: Number(tagID) }] })
      if (activity) {
        for (let i = 0; i < activity.tags.length; i++) {
          if (activity.tags[i].id === Number(tagID)) {
            resolve(activity.tags[i].tag)
          }
        }
      }
    })
  }

  getTags () {
    return new Promise((resolve, reject) => {
      const tagObjects = flatMapDeep(this.activities, 'tags')
      resolve(uniqBy(tagObjects, 'id'))
    })
  }

  getActivityByActivityID (id) {
    for (const activity of this.activities) {
      if (id === `${activity.id}`) {
        return activity
      }
    }
  }

  postLike (activityID) {
    return this._makeRequest('like', {
      method: 'POST',
      body: {
        id: activityID
      }
    }).then(res => {
      for (const activity of this.activities) {
        if (activity.id === activityID) {
          activity.likes = res.likes
          return res
        }
      }
    })
  }

  postDislike (activityID) {
    return this._makeRequest('dislike', {
      method: 'POST',
      body: {
        id: activityID
      }
    }).then(res => {
      for (const activity of this.activities) {
        if (activity.id === activityID) {
          activity.dislikes = res.dislikes
          return res
        }
      }
    })
  }

  login (username, password) {
    return this._makeRequest('login', {
      method: 'POST',
      body: {
        username: username,
        password: password
      }
    }).then(res => {
      this.setToken(res.token)
      return Promise.resolve(res)
    })
  }

  loggedIn () {
    const token = this.getToken()
    return token !== '' && token !== null && !this.isTokenExpired(token)
  }

  isTokenExpired (token) {
    try {
      const decoded = decode(token)
      return (decoded.exp < Date.now() / 1000)
    } catch (err) {
      console.warn(`Error: ${err}`)
      return false
    }
  }

  setToken (token) {
    localStorage.setItem('id', token)
    this._handleChange()
  }

  getToken () {
    return localStorage.getItem('id')
  }

  getName () {
    if (this.loggedIn()) {
      try {
        return decode(localStorage.getItem('id')).name
      } catch (err) {
        console.warn(`Error: ${err}`)
      }
    }
    return ''
  }

  getUsername () {
    if (this.loggedIn()) {
      try {
        return decode(localStorage.getItem('id')).username
      } catch (err) {
        console.warn(`Error: ${err}`)
      }
    }
    return ''
  }

  getID () {
    if (this.loggedIn()) {
      try {
        return decode(localStorage.getItem('id')).id
      } catch (err) {
        console.warn(`Error: ${err}`)
      }
    }
    return ''
  }

  logout () {
    localStorage.removeItem('id')
    this._handleChange()
  }

  postNewActivity (data) {
    return this._makeRequest('new-activity', {
      method: 'POST',
      body: data
    }).then((res) => {
      this.fetchActivities()
      return res
    })
  }

  subscribe (onChange) {
    this.onChange = onChange
  }

  _handleChange () {
    if (this.onChange !== null) this.onChange()
  }

  _makeRequest (url, options) {
    options.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }

    if (this.loggedIn()) {
      options.headers.Authorization = `Bearer ${this.getToken()}`
    }

    if (options.body) {
      options.body = JSON.stringify(options.body)
    }

    return fetch(API_SERVER_URL + url, options).then((response) => {
      if (response.ok) {
        return response.json()
      } else {
        return response.json().then(({ errors }) => {
          const error = new Error(response.statusText)
          response.errorMessages = errors
          throw error
        })
      }
    })
  }
}

export default HTTPService
