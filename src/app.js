import React from 'react'
import { Switch, BrowserRouter, Route } from 'react-router-dom'
import Home from './page/home'
import ActivityDetails from './page/activity/activity-details'
import PostNewActivity from './page/post-new-activity'
import Dashboard from './page/dashboard/dashboard'
import TagDashboard from './page/tag-dashboard'
import TopNavBar from './component/navigation/top-nav-bar'
import BottomNavBar from './component/navigation/bottom-nav-bar'
import Login from './page/login'

const App = (props) => {
  return (
    <div className='App'>
      <BrowserRouter>
        <TopNavBar httpService={props.httpService} />
        <div id='mainApp'>
          <Switch>
            <Route path='/login' component={() => <Login httpService={props.httpService} />} />
            <Route path='/post' component={() => <PostNewActivity httpService={props.httpService} />} />
            <Route path='/tags' component={() => <TagDashboard httpService={props.httpService} />} />
            <Route
              path='/dashboard/:tagID?' component={({ match }) =>
                <Dashboard
                  activities={props.httpService.getActivitiesByTagID(match.params.tagID)}
                  httpService={props.httpService}
                  tagID={match.params.tagID}
                />}
            />
            <Route exact path='/activity/:activityID' component={({ match }) => <ActivityDetails activity={props.httpService.getActivityByActivityID(match.params.activityID)} httpService={props.httpService} />} />
            <Route exact path='/' component={() => <Home />} />
          </Switch>
        </div>
        <BottomNavBar />
      </BrowserRouter>
    </div>
  )
}

export default App
