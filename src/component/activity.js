import React, { Component } from 'react'
import { Button } from '../component/button'
import './activity.css'
import LikeDislike from './like-dislike'

class Activity extends Component {
  constructor (props) {
    super(props)
    this.state = {
      httpService: this.props.httpService,
      activity: this.props.activity
    }
  }

  render () {
    if (this.state.activity) {
      return (
        <div className='activity'>
          <img
            className='activityImage'
            alt={`activity: ${this.state.activity.title}`}
            src={this.state.activity.imgURL}
          />
          <div className='activityContents'>
            <h1>{this.state.activity.title}</h1>
            <Button primary to={`/activity/${this.state.activity.id}`} label='See Details' />
            <LikeDislike
              activityID={this.state.activity.id}
              httpService={this.state.httpService}
              likes={this.state.activity.likes}
              dislikes={this.state.activity.dislikes}
            />
          </div>
        </div>
      )
    } else {
      return (
        <div className='activity'>
          <div className='activityContents'>
            <h1>Error</h1>
            <p>You should not see this!</p>
          </div>
        </div>
      )
    }
  }
}

export default Activity
