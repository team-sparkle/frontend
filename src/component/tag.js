import React, { Component } from 'react'
import './tag.css'
import { NavLink } from 'react-router-dom'

class Tag extends Component {
  constructor (props) {
    super(props)
    this.state = {
      tagName: this.props.tagName,
      tagID: this.props.tagID,
      redirectPath: `/dashboard/${this.props.tagID}`
    }
  }

  render () {
    if (this.state.tagID && this.state.tagName) {
      return (
        <NavLink to={this.state.redirectPath} className='tagCard'>
          <div className='tagCardContents'>
            <h1>{this.state.tagName}</h1>
          </div>
        </NavLink>
      )
    } else {
      return (
        <div className='tagCard'>
          <div className='tagCardContents'>
            <h1>Error</h1>
            <br />
            <p>You should not see this!</p>
          </div>
        </div>
      )
    }
  }
}

export default Tag
