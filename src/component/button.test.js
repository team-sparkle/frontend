import React from 'react'
import { configure, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Button } from './button'

configure({ adapter: new Adapter() })

describe('<Button />', () => {
  it('should mount without crashing', () => {
    mount(<Button />)
  })
})
