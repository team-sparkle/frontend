import React, { Component } from 'react'
import { TiArrowDownThick, TiArrowUpThick } from 'react-icons/ti'
import './like-dislike.css'

class LikeDislike extends Component {
  constructor (props) {
    super(props)
    this.state = {
      httpService: this.props.httpService,
      activityID: this.props.activityID,
      likes: this.props.likes,
      dislikes: this.props.dislikes
    }

    this.handleLike = this.handleLike.bind(this)
    this.handleDislike = this.handleDislike.bind(this)
  }

  handleLike (e) {
    this.state.httpService.postLike(this.state.activityID)
      .then(res => {
        this.setState({
          likes: res.likes
        })
      })
      .catch((err) => window.alert(`OOPS! Something Went Wrong!\n\n${err}`))
  }

  handleDislike (e) {
    this.state.httpService.postDislike(this.state.activityID)
      .then((res) => {
        this.setState({
          dislikes: res.dislikes
        })
      })
      .catch((err) => window.alert(`OOPS! Something Went Wrong!\n\n${err}`))
  }

  render () {
    return (
      <div className='right'>
        <TiArrowUpThick className='likePanelChild' onClick={this.handleLike} />
        {this.state.likes}
        <TiArrowDownThick className='likePanelChild' onClick={this.handleDislike} />
        {this.state.dislikes}
      </div>
    )
  }
}

export default LikeDislike
