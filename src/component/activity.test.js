import React from 'react'
import { configure, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Activity from './activity'

configure({ adapter: new Adapter() })

describe('<Activity />', () => {
  it('should mount without crashing', () => {
    mount(<Activity />)
  })
})
