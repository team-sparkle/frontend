import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import './button.css'

class Button extends Component {
  render () {
    if (this.props.primary) {
      if (this.props.to) {
        return (
          <NavLink to={this.props.to}>
            <button className='primaryButton'>{this.props.label}</button>
          </NavLink>
        )
      } else {
        return <button className='primaryButton'>{this.props.label}</button>
      }
    } else if (this.props.secondary) {
      if (this.props.to) {
        return (
          <NavLink to={this.props.to}>
            <button className='secondaryButton'>{this.props.label}</button>
          </NavLink>
        )
      } else {
        return <button className='secondaryButton'>{this.props.label}</button>
      }
    } else {
      return <button />
    }
  }
}

export { Button }
