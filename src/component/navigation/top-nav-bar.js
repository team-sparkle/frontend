import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import Logo from '../../assets/logo48.png'
import './navigation.css'

class TopNavBar extends Component {
  constructor (props) {
    super(props)

    this.state = {
      httpService: this.props.httpService
    }

    this.handleLogout = this.handleLogout.bind(this)
  }

  handleLogout () {
    this.state.httpService.logout()
  }

  render () {
    let loggedIn = false
    let name = ''
    if (typeof (this.state.httpService) !== 'undefined' && this.state.httpService.loggedIn()) {
      loggedIn = true
      name = this.state.httpService.getName()
    }

    return (
      <div className='topNavBar'>
        <div className='logo'>
          <NavLink exact to='/'>
            <img src={Logo} alt="Kid's Fort Logo" />
          </NavLink>
        </div>
        <div className='menuItem'>
          <NavLink exact to='/dashboard'>
            <h2 className='whiteText'>All Activities</h2>
          </NavLink>
        </div>
        <div className='menuItem'>
          <NavLink exact to='/tags'>
            <h2 className='whiteText'>All Tags</h2>
          </NavLink>
        </div>
        {!loggedIn &&
          <div className='menuItem right'>
            <NavLink exact to='/login'>
              <h2 className='whiteText'>Login</h2>
            </NavLink>
          </div>}
        {loggedIn &&
          <div className='menuItemWrapper right'>
            <div className='menuText'>
              <h2>{name}</h2>
            </div>
            <div className='menuItem' onClick={this.handleLogout}>
              <h2 className='whiteText'>Logout</h2>
            </div>
          </div>}
      </div>
    )
  }
}
export default TopNavBar
