import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import './navigation.css'

class BottomNavBar extends Component {
  render () {
    return (
      <div className='bottomNavBar'>
        <div className='copyright'>
          <p>Copyright May 2020 | Team Sparkle | <a href='https://gitlab.com/team-sparkle'>Gitlab</a></p>
        </div>
        <div className='contribute'>
          <NavLink exact to='/post'>
            <h2 className='whiteText'>Contribute</h2>
          </NavLink>
        </div>
      </div>
    )
  }
}
export default BottomNavBar
